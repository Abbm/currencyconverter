/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.currencyconverter;

import java.io.IOException;
import java.net.URISyntaxException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.simple.parser.ParseException;

/**
 * REST Web Service
 *
 * @author beatriz
 */
@Path("convert")
public class GenericResource {

    @Context
    private UriInfo context;
    CurrencyConverter currencyConverter;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
        this.currencyConverter = new CurrencyConverter();
    }

    /**
     * Retrieves representation of an instance of
     * com.mycompany.currencyconverter.GenericResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response getJSON(@QueryParam("from") String from, @QueryParam("to") String to, @QueryParam("amountFrom") Double amount) throws URISyntaxException, IOException, ParseException {
        double rate = currencyConverter.convertTax(from, amount, to);
        return Response.ok(rate, MediaType.TEXT_PLAIN).build();
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putJSON(String content) {
    }
}
