package com.mycompany.currencyconverter;

import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author beatriz
 */
public class CurrencyConverter {

    private double rateFrom = 0.0;

    private double rateTo = 0.0;

    private ApiInfo api = new ApiInfo();

    public double convertTax(String from, double amount, String to) throws URISyntaxException, IOException, ParseException {
        double tempRate = 0.0; //taxa temporaria 
        JSONObject obj = api.convert();
        for (Object currency : obj.keySet()) {

            String sub = currency.toString();

            if (from.equalsIgnoreCase("USD")) {  // se a conversão for feita de USD -> outro cambio
                if (to.equals(sub.substring(3))) {
                    rateTo = amount * (double) obj.get(currency);
                }
            } else {
                if (from.equalsIgnoreCase(sub.substring(3))) {

                    for (Object currencyTo : obj.keySet()) {
                        String sub1 = currencyTo.toString();
                        if (to.equalsIgnoreCase("USD")) {
                            rateTo = amount / (double) obj.get(currency);
                        } else if (to.equalsIgnoreCase(sub1.substring(3))) {
                            tempRate = 1 / (double) obj.get(currency);
                            rateFrom = amount * tempRate;
                            rateTo = rateFrom * (double) obj.get(currencyTo);
                        }

                    }
                }
            }

        }
        return rateTo;
    }

}
