/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.currencyconverter;

import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author beatriz
 */
public class ApiInfo {

    private static URIBuilder uriBuilder;

    public JSONObject convert() throws URISyntaxException, IOException, ParseException {

        //faz a conecção com a api para obter os dados
        uriBuilder = new URIBuilder("http://www.apilayer.net/api/live?access_key=776c730c05ed0702f65dd2c019c4eb86");

        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet request = new HttpGet(uriBuilder.build().toString());
        CloseableHttpResponse response = client.execute(request);

        HttpEntity entity = response.getEntity();
        String response1 = EntityUtils.toString(entity);

        //vai ao json buscar a informação 
        JSONObject obj = (JSONObject) new JSONParser().parse(response1);
        obj = (JSONObject) obj.get("quotes");

        return obj;
    }
}
