/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.currencyconverter;

import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author beatriz
 */
public class CurrencyConverterTest {

    public CurrencyConverterTest() {
        
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    

//    /**
//     * Test of convertTax method, of class CurrencyConverter.
//     */
    @Test
    public void testConvertTax() throws Exception {
        System.out.println("convertTax");
        String from = "EUR";
        String to = "USD";
        double amount = 2.0;
        CurrencyConverter cc = new CurrencyConverter();
        Object result = cc.convertTax(from, amount, to);
        assertTrue(result instanceof Double);

    }
    

}
