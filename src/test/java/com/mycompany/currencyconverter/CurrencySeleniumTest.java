/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.currencyconverter;

/**
 *
 * @author beatriz
 */

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class CurrencySeleniumTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/home/beatriz/Documentos/TQS/HW1/currencyconverter/src/main/resources/chromedriver");
        driver = new ChromeDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testHomeworkTQS() throws Exception {
        // ERROR: Caught exception [unknown command []]
        driver.get("http://localhost:8080/CurrencyConverter-1.0-SNAPSHOT/");
        driver.findElement(By.id("amountFrom")).click();
        driver.findElement(By.id("amountFrom")).clear();
        driver.findElement(By.id("amountFrom")).sendKeys("1");
        new Select(driver.findElement(By.id("from"))).selectByVisibleText("EUR");
        driver.findElement(By.id("from")).click();
        new Select(driver.findElement(By.id("to"))).selectByVisibleText("GBP");
        driver.findElement(By.id("to")).click();
        driver.findElement(By.id("convertBTN")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
